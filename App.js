import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import React from 'react';

const App = () => {
  const [firtsName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [fullName, setFullName] = React.useState('');
  const getFullName = () => {
    setFullName(`${firtsName} ${lastName}`);
  };
  return (
    <View>
      {/* text input */}
      <Text>Nama Depan</Text>
      <TextInput
        placeholder="First Name"
        onChangeText={firtsName => setFirstName(firtsName)}
      />
      <Text>Nama Belakang</Text>
      <TextInput
        placeholder="Last Name"
        onChangeText={lastName => setLastName(lastName)}
      />

      {/* button */}
      <TouchableOpacity
        onPress={() => getFullName()}
        style={{
          backgroundColor: 'lightblue',
          marginHorizontal: 20,
          height: 40,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>Nama Lengkap</Text>
      </TouchableOpacity>

      <Text>{fullName}</Text>
    </View>
  );
};

export default App;
